package org.mcsg.system_control.command;


import org.bukkit.entity.Player;
import org.bukkit.command.util.ArrayUtil;

public abstract class PlayerCommand extends RootCommand{
	
	public boolean call(Player player, String[] args){
		if(args.length > 0){
			if(subs.containsKey(args[0].toLowerCase())){
				return subs.get(args[0].toLowerCase()).onCommand(player, ArrayUtil.cutArray(args, 1));
			} else {
				return onCommand(player, args);
			}
		} else {
			return onCommand(player, args);
		}
	}
	
	public abstract boolean onCommand(Player player, String[] args);

	
}
