package org.mcsg.system_control.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.command.util.ArrayUtil;

public abstract class ConsoleCommand extends RootCommand{

	
	public boolean call(CommandSender sender, Player player, String[] args){
		if(args.length > 0){
			if(subs.containsKey(args[0].toLowerCase())){
				return subs.get(args[0].toLowerCase()).onCommand(player, ArrayUtil.cutArray(args, 1));
			} else {
				return onCommand(sender, player, args);
			}
		} else {
			return onCommand(sender, player, args);
		}
	}
	
	public abstract boolean onCommand(CommandSender sender, Player player, String[] args);


}
