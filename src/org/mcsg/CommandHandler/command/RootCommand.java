package org.mcsg.system_control.command;

import java.util.HashMap;

import org.bukkit.command.CommandSender;

public abstract class RootCommand {

	HashMap<String, SubCommand> subs = new HashMap<String, SubCommand>();

	public RootCommand(){
		registerCommands();
	}
	
	public void registerCommand(String name, SubCommand command){
		subs.put(name, command);
	}
	
	public abstract void registerCommands();
	
	public void listSubCommands(CommandSender sender){
		
	}
}
