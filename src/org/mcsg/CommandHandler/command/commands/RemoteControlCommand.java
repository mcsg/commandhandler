package org.mcsg.system_control.command.commands;

import java.util.Arrays;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.api.managment.Monitor;
import org.mcsg.api.managment.RemoteControl;
import org.mcsg.api.util.StringUtil;
import org.mcsg.system_control.command.ConsoleCommand;
import org.mcsg.system_control.command.SubCommand;
import org.mcsg.system_control.util.PermissionUtil;

import static org.mcsg.system_control.util.PermissionUtil.hasPermission;

public class RemoteControlCommand extends ConsoleCommand{

	@Override
	public boolean onCommand(CommandSender sender, Player player, String[] args) {
		listSubCommands(sender);
		return false;
	}

	@Override
	public void registerCommands() {
		registerCommand("shutdown", new ShutDown());
		registerCommand("reload", new Reload());
		registerCommand("alert", new Alert());
		registerCommand("message", new Message());
		registerCommand("command", new ServerCommand());

	}

	public class ShutDown implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(hasPermission(player, "sc.rc.shutdown")){
				RemoteControl.shutdown(Monitor.getInstance().getServers(args));
				return true;
			} else {
				return false;
			}
		}

		@Override
		public String help() {
			return null;
		}
		
	}
	
	public class Reload implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(hasPermission(player, "sc.rc.reload")){
				RemoteControl.reload(Monitor.getInstance().getServers(args));
				return true;
			} else {
				return false;
			}
		}

		@Override
		public String help() {
			return null;
		}
		
	}
	
	public class Alert implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(hasPermission(player, "sc.rc.alert")){
				RemoteControl.alert(Monitor.getInstance().getServers(new String[]{"**"}), StringUtil.arrayToString(args));
				return true;
			} else {
				return false;
			}
		}

		@Override
		public String help() {
			return null;
		}
		
	}
	
	public class Message implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.message")){
				RemoteControl.message(player.getName(), args[0], StringUtil.arrayToString(Arrays.copyOfRange(args, 1, args.length)));
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stubBOtj
			
			return null;
		}
		

		
	}
	
	public class ServerCommand implements SubCommand{
		
		public boolean onCommand(Player player, String[] args){
			if(PermissionUtil.hasPermission(player, "sc.command")){
				List<String> servers = Monitor.getInstance().getServers(args[0].split(","));
				String command = StringUtil.arrayToString(Arrays.copyOfRange(args, 1, args.length));
				RemoteControl.runCommand(servers, command);
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
