package org.mcsg.system_control.command.commands;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.api.API;
import org.mcsg.api.managment.Monitor;
import org.mcsg.api.managment.NetworkServer;
import org.mcsg.api.managment.NetworkServer.Status;
import org.mcsg.system_control.command.ConsoleCommand;
import org.mcsg.system_control.command.SubCommand;
import org.mcsg.system_control.util.PermissionUtil;

public class GListCommand extends ConsoleCommand{

	@Override
	public boolean onCommand(CommandSender sender, Player player, String[] args) {
		if (player != null && !PermissionUtil.hasPermission(player, "api.glist")) {
			player.sendMessage(ChatColor.RED+"You do not have sufficient permissions");
			
			return false;
		}
		
		List<NetworkServer> servers = Monitor.getInstance().getServers(Status.ONLINE, Status.DEADLOCK);
		
		StringBuilder builder = new StringBuilder();
		for(NetworkServer server : servers){
			builder.append(ChatColor.GREEN+"["+server.getName()+": "+ChatColor.AQUA+server.getOnline()+ChatColor.GREEN+"] ");
			
			String[] clients = server.getPlayers();
			for (int i =0;i<clients.length;i++) {
				builder.append(ChatColor.YELLOW+clients[i]);
				
				if (i < clients.length-1) { 
					builder.append(", ");
				}
			}
		}
		sender.sendMessage(builder.toString());
		
		return true;
	}

	@Override
	public void registerCommands() {
		registerCommand("count", new Count());
		
	}
	
	class Count implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			boolean console = player == null;
			
			if (!console && !PermissionUtil.hasPermission(player, "api.glist")) {
				player.sendMessage(ChatColor.RED+"You do not have sufficient permissions");
				
				return false;
			}
			List<NetworkServer> servers = Monitor.getInstance().getServers(Status.ONLINE, Status.DEADLOCK);
			
			StringBuilder builder = new StringBuilder();
			builder.append(ChatColor.AQUA+"There are: "+ChatColor.GREEN+Monitor.getInstance().getNetworkPlayerCount()+ChatColor.AQUA+" players on the MC-SG network.\n");
			for(NetworkServer server : servers) {
				builder.append(ChatColor.GREEN+server.getName()+ChatColor.YELLOW+" has "+ChatColor.AQUA+server.getOnline()+ChatColor.YELLOW+" players online.\n");
			}
			if (console) {
				API.$("", builder.toString());
			}
			else {
				player.sendMessage(builder.toString());
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
	}
}
