package org.mcsg.system_control.command.commands;

import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.api.managment.Monitor;
import org.mcsg.api.managment.NetworkServer;
import org.mcsg.api.managment.NetworkServer.Status;
import org.mcsg.api.util.MessageUtil;
import org.mcsg.system_control.command.ConsoleCommand;

public class Find extends ConsoleCommand{

	@Override
	public boolean onCommand(CommandSender sender, Player player, String[] args) {
		String needle = args[0];
		
		List<NetworkServer> servers = Monitor.getInstance().getServers(Status.ONLINE, Status.DEADLOCK);
		
		NetworkServer found = null;
		for(NetworkServer server : servers){
			for(String p : server.getPlayers()){
				if(p.equalsIgnoreCase(needle)){
					found = server;
					break;
				}
				if(found != null){
					break;
				}
			}
		}
		
		if(found != null){
			sender.sendMessage(MessageUtil._("&ePlayer &6{0}&e is online on the server &6{1}",needle, found.getName()));
			return true;
		} else {
			sender.sendMessage(MessageUtil._("&cPlayer &6{0} &cis not online", needle));
			return false;
		}
		
	}

	@Override
	public void registerCommands() {
		// TODO Auto-generated method stub
		
	}

}
