package org.mcsg.system_control.command.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.api.API;
import org.mcsg.api.lilypad.LinkerServer;
import org.mcsg.system_control.command.ConsoleCommand;

public class DebugCommand extends ConsoleCommand{

	@Override
	public boolean onCommand(CommandSender sender, Player player, String[] args) {
		if(args[0].equalsIgnoreCase("all")){
			API.setDebugEnabled(true);
			LinkerServer.getInstance().enableDebug();
		}
		else if(args[0].equalsIgnoreCase("on")){
			API.setDebugEnabled(true);
			sender.sendMessage("on");
		} else {
			API.setDebugEnabled(false);
			LinkerServer.getInstance().disableDebug();
			sender.sendMessage("off");
		}
		return true;
		
	}

	@Override
	public void registerCommands() {
		// TODO Auto-generated method stub
		
	}

}
