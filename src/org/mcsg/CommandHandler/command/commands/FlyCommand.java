package org.mcsg.system_control.command.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mcsg.api.lib.util.DoubleJump;
import org.mcsg.system_control.command.PlayerCommand;
import org.mcsg.system_control.util.PermissionUtil;

public class FlyCommand extends PlayerCommand{



	@Override
	public boolean onCommand(Player player, String[] args) {
		if(PermissionUtil.hasPermission(player, "essentials.fly")){
			if(DoubleJump.isDoubleJumping(player)){
				DoubleJump.setDoubleJump(player, false);
			} else {
				DoubleJump.setDoubleJump(player, true);
			}
			player.sendMessage(ChatColor.GREEN+"Fly toggled");
		}
		return true;
	}

	@Override
	public void registerCommands() {
		// TODO Auto-generated method stub

	}

}
