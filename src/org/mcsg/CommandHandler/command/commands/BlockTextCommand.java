package org.mcsg.system_control.command.commands;

import org.bukkit.entity.Player;
import org.mcsg.api.lib.blocktext.BlockCharacter.RenderMode;
import org.mcsg.api.lib.blocktext.BlockText.Align;
import org.mcsg.system_control.TextWallManager;
import org.mcsg.system_control.command.PlayerCommand;
import org.mcsg.system_control.command.SubCommand;
import org.mcsg.system_control.util.PermissionUtil;

public class BlockTextCommand extends PlayerCommand{

	
	
	
	@Override
	public boolean onCommand(Player player, String[] args) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void registerCommands() {
		registerCommand("addwall", new addBlock());
		registerCommand("addtext", new addText());
		registerCommand("resettext", new resetText());
		registerCommand("removewall", new removeBlock());
		registerCommand("setdelay", new setDelay());
		registerCommand("setmode", new setMode());
		registerCommand("setalign", new setAlign());

	}
	
	class addBlock implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.blocktext")){
				TextWallManager.getInstance().addWall(args[0], args[1], args[2], player.getLocation());
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	class removeBlock implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.blocktext")){
				TextWallManager.getInstance().removeWall(args[0]);
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	class resetText implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.blocktext")){
				TextWallManager.getInstance().getWall(args[0]).resetText();
				}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	class addText implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.blocktext")){
				StringBuilder sb = new StringBuilder();
				for(int a = 1; a < args.length; a++){
					sb.append(args[a]).append(" ");
				}
				TextWallManager.getInstance().getWall(args[0]).addText(sb.toString());
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	class setDelay implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.blocktext")){
				TextWallManager.getInstance().getWall(args[0]).setDelay(Integer.parseInt(args[1]));
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	class setAlign implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.blocktext")){
				TextWallManager.getInstance().getWall(args[0]).setAlign(Align.valueOf(args[1]));
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	
	class setMode implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.blocktext")){
				TextWallManager.getInstance().getWall(args[0]).setMode(RenderMode.valueOf(args[1]));
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}

}
