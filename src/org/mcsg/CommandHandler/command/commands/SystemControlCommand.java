package org.mcsg.system_control.command.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.api.fileserver.FileSyncServer;
import org.mcsg.api.lilypad.LinkerServer;
import org.mcsg.system_control.command.ConsoleCommand;
import org.mcsg.system_control.command.SubCommand;
import org.mcsg.system_control.util.PermissionUtil;

public class SystemControlCommand extends ConsoleCommand{

	@Override
	public boolean onCommand(CommandSender sender, Player player, String[] args) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void registerCommands() {
		this.registerCommand("server", new Server());
		this.registerCommand("freeze", new Freeze());

	}
	
	
	
	class Server implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.server.tp.unrestricted")){
				LinkerServer.getInstance().redirectRequest(args[0], player.getName(), 5);
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}

	class Sync implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.sync")){
				new FileSyncServer(args[0]).checkForUpdates(false).sync(true);
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	class Freeze implements SubCommand{

		@Override
		public boolean onCommand(Player player, String[] args) {
			if(PermissionUtil.hasPermission(player, "sc.freeze")){
				try{ Thread.sleep(Long.parseLong(args[0])); } catch (Exception e){}
			}
			return true;
		}

		@Override
		public String help() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}

}
