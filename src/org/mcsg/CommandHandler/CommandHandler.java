package org.mcsg.system_control;

import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.system_control.command.ConsoleCommand;
import org.mcsg.system_control.command.PlayerCommand;
import org.mcsg.system_control.command.RootCommand;


public class CommandHandler implements CommandExecutor{

	private static CommandHandler instance = new CommandHandler();

	private CommandHandler(){

	}

	public static CommandHandler getInstance(){
		return instance;
	}

	private static HashMap<String, RootCommand> commands = new HashMap<String, RootCommand>();

	public void registerCommand(String name, RootCommand command){
		SystemControl.getPlugin().getCommand(name).setExecutor(instance);
		commands.put(name,  command);
	}


	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(commands.containsKey(cmd.getName())){
			Player player = null;
			if(sender instanceof Player){
				player = (Player)sender;
			} 

			RootCommand command = commands.get(cmd.getName());
			if(command instanceof ConsoleCommand){
				ConsoleCommand cc = (ConsoleCommand)command;
				return  cc.call(sender, player, args);
			} else {
				if(player == null){
					sender.sendMessage("Cannot run this command from console");
					return false;
				} else {
					PlayerCommand pc = (PlayerCommand)command;
					return pc.call(player, args);
				}
			}

		} else {
			return false;
		}
	}
}
